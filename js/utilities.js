function enable_disable_Button() {

    let button_add = document.getElementsByTagName('button')[0];

    if(button_add.disabled) {
        button_add.disabled = false;
    } else {
        button_add.disabled = true;
    }
}

function set_elem_required(element_id, image) {

    let element = document.getElementById(element_id);
    element.setAttribute('required', '');

    let labels = document.getElementsByTagName('label');
    let label = null;

    for(let i = 0; i < labels.length; i++) {
        if(labels[i].htmlFor == element_id) {
            label = labels[i];
        }
    }

    label.appendChild(image);

    /*label.style.fontWeight = bold;*/
}

function createTableFromArrayObjects(project_array, parent_id) {

    let parent = document.getElementById(parent_id);

    project_array.forEach(proj => {
        let row = document.createElement('tr');

        Object.values(proj).forEach(text => {
            let cell = document.createElement('td');
            let Txt_node = document.createTextNode(text);
            cell.appendChild(Txt_node);
            row.appendChild(cell);
            i++;
        });
        
        parent.appendChild(row);
        
    });
}

function validateElement(regex, str) {
    
    if(!regex.test(str)) {
        return true;
    } else {
        return false;
    }
}

function setElementFeedback(element_id, validation_condition) {

    let image_wrong = document.createElement("img");
    image_wrong.src = "../images/wrong.PNG";

    let image_valid = document.createElement("img");
    image_valid.src = "../images/valid.PNG";

    if(validation_condition) {
        set_elem_required(element_id, image_wrong);
    } else {
        set_elem_required(element_id, image_valid);
    }

}