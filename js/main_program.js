"uses strict";

let all_projects_arr = [];
let regex_projId = /^[a-zA-Z][a-zA-Z0-9/&_]{2,10}$/;
let regex_ownerN = /^[a-zA-Z][a-zA-Z0-9/]{2,10}$/;
let regex_ProjT = /[a-zA-Z0-9]{3,25}$/;
let regex_NberAndRate = /^[0-9]{1,3}$/;
let regex_ProjDesc = /^[a-zA-Z]{3,65}$/;



document.addEventListener('DOMContentLoaded', function() {

    enable_disable_Button();

    let project_id = document.getElementById('project_id');
    project_id.addEventListener("blur", validateProjId);

    let owner_name = document.getElementById('owner_name');
    owner_name.addEventListener("blur", validateOwnerName);

    let title = document.getElementById('title');
    title.addEventListener("blur", validateTitle);
    /*
    let category = document.getElementById('category');
    category.addEventListener("blur", validateAll);
    */
    let hours = document.getElementById('hours');
    hours.addEventListener("blur", validateHours);

    let rate = document.getElementById('rate'); 
    rate.addEventListener("blur", validateRate);
    /*
    let status = document.getElementById('status');
    status.addEventListener("blur", validateAll);
    */
    let description = document.getElementById('s-desc');
    description.addEventListener("blur", validateDesc);

});

function validateRate() {

    if(document.getElementsByTagName('label')[5].childElementCount == 1) {
        document.getElementsByTagName('label')[5].childNodes[1].remove();
    }
    
    setElementFeedback('rate',validateElement(regex_NberAndRate,getRate()));
}
function validateDesc() {

    if(document.getElementsByTagName('label')[7].childElementCount == 1) {
        document.getElementsByTagName('label')[7].childNodes[1].remove();
    }

    setElementFeedback('s-desc',validateElement(regex_ProjDesc,getDescription()));
}
function validateTitle() {

    if(document.getElementsByTagName('label')[2].childElementCount == 1) {
        document.getElementsByTagName('label')[2].childNodes[1].remove();
    }

    setElementFeedback('title',validateElement(regex_ProjT,getTitle()));
}
function validateHours() {

    if(document.getElementsByTagName('label')[4].childElementCount == 1) {
        document.getElementsByTagName('label')[4].childNodes[1].remove();
    }

    setElementFeedback('hours',validateElement(regex_NberAndRate,getHours()));
}
function validateOwnerName() {

    if(document.getElementsByTagName('label')[1].childElementCount == 1) {
        document.getElementsByTagName('label')[1].childNodes[1].remove();
    }

    setElementFeedback('owner_name',validateElement(regex_ownerN, getOwner_Name()));
}
function validateProjId() {

    if(document.getElementsByTagName('label')[0].childElementCount == 1) {
        document.getElementsByTagName('label')[0].childNodes[1].remove();
    }

    setElementFeedback('project_id',validateElement(regex_projId,getProject_Id()));
}


 

