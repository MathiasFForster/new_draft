function createProjectObject() {

    let project = new Object();
    project.projectId = getProject_Id();
    project.onwerName = getOwner_Name();
    project.title = getTitle();
    project.category = getCategory();
    project.status = getStatus();
    project.hours = getHours();
    project.rate = getRate();
    project.description = getDescription();

    return project;
}

function deleteTableRow() {

    let deletion = "Do you want to delete this project?";

    if(confirm(deletion) == true) {

    }
}

function updateProjectsTable() {


}

function getProject_Id() {
    let p_id = document.getElementById('project_id').value;
    return p_id;
}
function getOwner_Name() {
    let o_name = document.getElementById('owner_name').value;
    return o_name;
}
function getTitle() {
    let title = document.getElementById('title').value;
    return title;
}
function getCategory() {
    let category = document.getElementById('category').value;
    return category;
}
function getHours() {
    let hours = document.getElementById('hours').value;
    return hours;
}
function getRate() {
    let rate = document.getElementById('rate').value;
    return rate;
}
function getStatus() {
    let status = document.getElementById('status').value;
    return status;
}
function getDescription() {
    let description = document.getElementById('s-desc').value;
    return description;
}